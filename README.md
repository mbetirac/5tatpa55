# 5tatPa55

Effectue des statistiques sur une liste de mots de passe, notamment: 
* Compte le nombre d'occurences de mots non-uniques dans un fichier.
* Vérifie la robustesse des mots de passes dans la liste.
* Vérifie si les mots de passe ont fuité (via l'API d'HaveIbeenPwned) 

## Usage

Le format du fichier en entrée doit être composé d'un mot de passe par ligne.

```bash
python3 5tatPa55.py [-h] -f /path/to/file [-m MIN_VALUE] [-s] [-L] [-S ":"]
```

|    **Option**   	| **Description**                                                                                                         	|
|:---------------:	|-------------------------------------------------------------------------------------------------------------------------	|
| -h, --help      	| Affiche l'aide                                                                                                          	|
| -f, --file      	| Chemin vers le fichier à analyser                                                                                       	|
| -m, --min       	| Valeur minimale retournée par le résultat (Défaut = 2)                                                                  	|
| -s, --stats     	| Vérifie le nombre de mots de passe faibles contenus dans le fichier source                                              	|
| -L, --leaks     	| Utilise l'API d'HaveIbeenPwned pour savoir quels mots de passe ont fuité                                                	|
| -S, --separator 	| Si plus d'un mot de passe est présent par ligne, l'option -S permet de définir le séparateur utilisé (Par défaut = ",") 	|
| -c, --charts     	| Crée des graphs des mots de passes analysés. Ne fonctionne que si les options -s **et** -L sont invoquées                     |

## Dependencies

* Installer les dépendances fournies dans le fichier requirements: 

    ```bash
    pip install -r requirements.txt
    ```

* Les dépendances installées sont les suivantes :

    * matplotlib==3.6.1
    * prettytable==3.4.1
    * progress==1.6
    * requests==2.25.1
    

* Un accès internet fonctionnel est requis pour utiliser [**l'API**](https://haveibeenpwned.com/API/v2#PwnedPasswords) d'HaveIbeenPwned.

## Example

Un exemple de résultat de 5tatPa55:

![5tatPa55](img/5tatPa55-fullops.png)

