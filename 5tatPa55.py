import argparse
import re
import requests
import hashlib
from prettytable import PrettyTable
from progress.spinner import Spinner
import matplotlib.pyplot as plt

# Gestion des arguments pour utilisation correcte
parser = argparse.ArgumentParser(description="5tatPa55 : Compte le nombre d'occurences de mots non-uniques dans un fichier")
parser.add_argument("-f", "--file", help="Chemin vers le fichier à analyser", dest='path', required=True)
parser.add_argument("-m","--min", dest='min_value', type=int, default=2, help='Valeur minimale retournée par le résultat (Défaut = 2)')
parser.add_argument("-s", "--stats", help="Vérifie le nombre de mots de passe faibles contenus dans le fichier source", required=False, action="store_true")
parser.add_argument("-c", "--charts", help="Crée des graphs des mots de passes analysés", required=False, action="store_true")
parser.add_argument("-L", "--leaks", help="Utilise l'API d'HaveIbeenPwned pour savoir quels mots de passe ont fuité", required=False, action="store_true")
parser.add_argument("-S","--separator", dest='separator', type=str, default=",", help='Séparateur utilisé si plusieurs mot de passes sont sur la même ligne (Défaut = ,)')
args = parser.parse_args()
path = args.path

class bcolors:
    OKGREEN = '\033[92m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

try:
	print("	  										   ")
	print("	 ____  _        _               ____ ____  ")
	print("	| ___|| |_ __ _| |_ _ __   __ _| ___| ___| ")
	print("	|___ \| __/ _` | __| '_ \ / _` |___ \___ \ ")
	print("	 ___) | || (_| | |_| |_) | (_| |___) |__) |")
	print("	|____/ \__\__,_|\__| .__/ \__,_|____/____/ ")
	print("	                   |_|                     ")
	print("	                                           ")
	print("		Maxime BETIRAC - @maddmhax      	   ")
	print("											   ")
	print("	[+] Analyse du fichier " + path)
	print("											   ")

	# Initialisation des variables et Regex
	weakpass_pattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
	weakpass = 0
	strongpass = 0
	weakpercent = 0
	strongpercent = 0
	totalwords = 0
	processing = 0
	passleaked = 0
	passnotleaked = 0
	tableausimple = PrettyTable()
	tableauleak = PrettyTable()
	tableaustat = PrettyTable()
	tableausimple.field_names = ["Mot de passe récurrents", "Occurence"]
	tableauleak.field_names = ["Mot de passe récurrents", "Occurence", "Etat sanité"]
	tableaustat.field_names = ["Mot de passe totaux", "Robustesse", "Réparition %"]
	separator = args.separator

	# Modifications du fichier si option -S est invoquée
	if args.separator:
		f1=open(path,"r+")
		input=f1.read()
		input=input.replace(separator,'\n')
		f2=open(path,"w+")
		f2.write(input)
		f1.close()
		f2.close()

	# Ouverture du fichier pour le placer dans un dictionnaire
	text = open(path, "r")
	passwd = text.read().split('\n')
	text.close()
	d = dict()
	
	# Loop sur chaque ligne du fichier
	for line in passwd:
		if len(line) >= 1: 	
			# Check la force des mots de passe :
			if re.match(weakpass_pattern, line):
				strongpass = strongpass + 1
			else:
				weakpass = weakpass + 1

			# Check si le mot est déjà dans le dictionnaire
			if line in d:
				# Incrémente l'occurence de 1 
				d[line] = d[line] + 1
			else:
				# Sinon, laisse l'occurence à 1
				d[line] = 1

			# Prise d'infos à chaque passage de boucle pour calcul plus tard + ajout pour chart
			totalwords = totalwords + 1
			if (d[line] >= args.min_value) and (len(line) >= 1) :
				plt.barh([line], [d[line]], color = 'r')

	plt.xlabel('Occurences')
	plt.ylabel('Mots de passes')
	# Giving the tilte for the plot
	plt.title('Occurences trouvées')

	plt.savefig("chart.png", bbox_inches='tight')
	# Trie le dictionnaire dans l'ordre du plus gros nombre d'occurence au moins nombreux
	sorted_values = dict(sorted(d.items(), key=lambda item: item[1], reverse=True))

	# Si argument demande les leaks
	if args.leaks:
		state = 'WORKING'
		# Spinner pour confirmer que l'opération est toujours en cours
		spinner = Spinner('		Analyse en cours ')
		while state != 'FINISHED':
			for key in list(sorted_values):
				# Vérifie la cohérence avec l'argument min_value et que le mot de passe fait bien plus d'un caractère
				if (d[key] >= args.min_value) and (len(key) >= 1) :
					
					# Transforme le pass en hash sha1 pour utiliser l'API
					hash_object = hashlib.sha1(key.encode('utf-8'))
					hex_dig = hash_object.hexdigest()
					# Garde que les 5 premiers caractères pour API
					hashfive = hex_dig[:5]
					response = requests.get("https://api.pwnedpasswords.com/range/" + hashfive)
					apihash = hex_dig[5:]
					# Test pour voir si sha1 fait partie de la liste retournée
					if re.search(apihash, response.text, re.IGNORECASE):
						tableauleak.add_row([key, d[key], bcolors.FAIL + bcolors.BOLD + '💀 L34k3D' + bcolors.ENDC])
						passleaked = passleaked + 1
					else:
						tableauleak.add_row([key, d[key], bcolors.OKGREEN + "👌 Sain" + bcolors.ENDC]) 
						passnotleaked = passnotleaked + 1
					spinner.next() 
				state = 'FINISHED'
		print("											   ")
		print(tableauleak)
		print("											   ")

	# Sinon affiche le tableau simple
	else: 
		for key in list(sorted_values):
			if (d[key] >= args.min_value) and (len(key) >= 1): 
				tableausimple.add_row([key, d[key]])
		print(tableausimple)

	# Si --stats/-s spécifié, effectue des calculs 
	if args.stats:
		weakpercent = weakpass * 100 / totalwords
		strongpercent = strongpass * 100 / totalwords
		# Puis provisionning/affichage  
		tableaustat.add_row([strongpass, "Forte", round(strongpercent)])
		tableaustat.add_row([weakpass, "Faible", round(weakpercent)])
		print("												   ")
		print(tableaustat)
		print("												   ")

	# PIE CHARTS
	if (args.leaks and args.stats and args.charts):

		# Pie chart SANITE
		labels = 'Sain', 'Leaked'
		sizes = [passnotleaked, passleaked]
		colors = ['green', 'red']
		explode = (0, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')
		fig1, ax1 = plt.subplots()
		ax1.pie(sizes, explode=explode, labels=labels, autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '', colors=colors, shadow=True, startangle=90)
		ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
		ax1.set_title("Etat des mots de passe analysés")
		# Save image as png
		plt.savefig("pieLeaked.png", transparent=True)

		# Pie chart ROBUSTESSE
		labels = 'Faible', 'Forte'
		sizes = [weakpercent, strongpercent]
		explode = (0.1, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')
		colors = ['red','green']
		fig1, ax1 = plt.subplots()
		ax1.pie(sizes, explode=explode, labels=labels, autopct=lambda p: '{:.1f}%'.format(round(p)) if p > 0 else '', colors=colors, shadow=True, startangle=90)
		ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
		ax1.set_title("Robustesse des mots de passe analysés")
		# Save image as png
		plt.savefig("pieRobustesse.png", transparent=True)

		print("Graphiques pieRobustesse.png & pieLeaked.png disponibles dans le dossier courant")

# Si erreurs dans le code on ne l'éxecute pas, très souvent à cause du fichier ou de la connexion internet :
except:
	print("❌ Impossible d'ouvrir le fichier")
	print("📶 Si l'option -L est invoquée, vérifiez aussi votre connexion internet")
